# Bash script to get incron for send Rclone sync on cloud

## Description

This script is call from an incron event as an executable Bash script to manage rclone sync.

## How to use

1. You have to install [incron](https://github.com/ar-/incron) from your package manager or from source

1. You have to setup incron..

	```

	su

	echo $USER | tee -a > /etc/incron.allow

	systemctl enable --now incrond

	exit

	```

1. Next step to add a survey task to incron..

	```

	incrontab -e

	```

1. from your opened TUI editor, add a line to target directory, event to control and command to call like:..

	```

	/home/my_user_name_/Images IN_CREATE,IN_ODIFY,IN_MOVED_FROM /home/my_user_name/my_script_dir/sync_images_dreopbox_pictures.sh

	```

1. make the script runnable..

	```

	chmod 755 synch_images_dropbox_pictures.sh

	```

1. create the log file..

	```

	mkdir ~/Logs

	touch rclone_sync_Dropbox_Pictures.log

	```

1. Install rclone (if not allready installed)

1. Setup a remote for dropbox or any other cloud 

1. modify your scipt to target to your own remote name and directory destination

Feel free to modify this script for your current use and enjoy.

