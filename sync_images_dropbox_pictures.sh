#! /bin/bash

# acquire a locker as long as this script file is running, 
# and exit if it is allready running
[ "${FLOCKER}" != "$0" ] && exec env FLOCKER="$0" flock -en "$0" "$0" "$@" || :
LOG_FILE=$HOME/Logs/rclone_sync_Dropbox_Pictures.log

sync_dropbox () {
  rclone sync \
    --log-file $LOG_FILE \
    --log-level INFO \
    $HOME/Images \
    dropbox-Jerome:/Pictures
}

send_message () {
  sudo -u jerome \
    DISPLAY=:0 \
    DBUS_SESSION_BUS_ADDRESS=unix:path=/run/user/1000/bus \
    notify-send -t 2000 "Synch Dropbox" "$@"
}

log_only_last_lines () {
  lines="$(wc -l < $LOG_FILE)"
  if [[ $lines -gt $1 ]]; then
    last_lines=$(echo "$lines - 100" | bc)
    tail -n +$last_lines $LOG_FILE > /tmp/dropbox.log && mv /tmp/dropbox.log $LOG_FILE
  fi
}

log_only_this_day () {
  this_day="$(date +'%Y/%m/%d')"
  first_line="$(awk -v td=$this_day '$1~td {p=NR; exit} END { print p ? p : 1 }' $1)"
  tail -n +$first_line $1 > /tmp/dropbox.log && mv /tmp/dropbox.log $1
}

# show message command start
send_message "rclone is starting to sync \non Dropbox for Pictures"

# test if rclone is not running and then if rclone job doesn't give back an error
if [[ -z $(pgrep -x "rclone") ]] && sync_dropbox; then
  # if no err, do it again to be sure nothing has been removed or added bfore to finish
  sync_dropbox
  # and show success message
  send_message "Synch for Images to Dropbox:/Pictures\nSUCCESS"
else
  # show failed message because something was wrong there with rclone job
  send_message "Sync for Images to Dropbox:/Pictures\nFAILED"
fi
#log_only_last_lines 100
log_only_this_day $LOG_FILE

